FROM php:7.4.1-fpm-alpine3.11

RUN apk update && apk upgrade
RUN docker-php-ext-install pdo_mysql 
RUN apk add composer
RUN apk add nodejs npm

ARG APP_UID=1000
ARG APP_GID=1000

RUN addgroup -g $APP_GID -S $APP_UID
RUN adduser -u $APP_UID -S $APP_UID -G $APP_GID

WORKDIR /var/www/html/

COPY ./app/ app/
COPY ./bootstrap/ bootstrap/
COPY ./config/ config/
COPY ./database/ database/
COPY ./public/ public/
COPY ./resources/ resources/
COPY ./routes/ routes/
COPY ./storage/ storage/
COPY ./tests/ tests/
COPY ./vendor/ vendor/

COPY ./.env .env
COPY ./.styleci.yml .styleci.yml
COPY ./artisan artisan
COPY ./composer.json composer.json
COPY ./composer.lock composer.lock
COPY ./package.json package.json 
COPY ./phpunit.xml phpunit.xml
COPY ./server.php server.php
COPY ./webpack.mix.js webpack.mix.js

RUN chown -R $APP_UID:$APP_GID .

USER $APP_GID

CMD php artisan serve --host=0.0.0.0 --port 8080