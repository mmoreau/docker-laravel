# Docker-Laravel

**Laravel 6.8.0** + **PHP-FPM 7.4.1** + **MySQL 8.0.18** + **PHPMyAdmin 5.0**

----

## Install Docker-CE

### Ubuntu
```
   $ sudo apt update
   $ sudo apt install apt-transport-https ca-certificates gnupg-agent curl software-properties-common
   $ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
   $ sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu (lsb_release -cs) stable"
   $ sudo apt update
   $ sudo apt-cache policy docker-ce
   $ sudo apt install docker-ce
   $ sudo systemctl status docker
   $ sudo usermod -aG docker myusername
   $ su myusername
   $ id -nG
   $ docker version
```

OR 

**Warning** : the **docker.io** package is not always the latest version and may have some problems with the latest version of **docker-compose**

```
   $ sudo apt-get install docker.io
   $ sudo usermod -aG docker myusername
   $ su myusername
   $ id -nG
   $ docker version
```

Restart the system

If you want to delete Docker-CE

```
   $ sudo apt remove docker-ce
   $ sudo apt-get autoremove
```

## Install Docker-Compose

### Ubuntu
```
   $ sudo curl -L "https://github.com/docker/compose/releases/download/1.25.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
   $ sudo chmod +x /usr/local/bin/docker-compose
   $ docker-compose version
```

If you want to delete docker-compose 

```
   $ sudo rm -rf /usr/local/bin/docker-compose
```

## Install Laravel / MySQL / PHPMyAdmin

Add ports 8000 & 8001 to Linux firewall rules

Open a new terminal and type this :

```
   $ mkdir -p ~/Desktop/laravel/blog
   $ docker run --rm -it -v ~/Desktop/laravel:/app/ --user $(id -u):$(id -g) composer:1.9.1 composer create-project --prefer-dist laravel/laravel blog
   $ cd ~/Desktop/laravel/blog
   $ curl https://gitlab.com/mmoreau/docker-laravel/raw/master/dev/app.dockerfile -o app.dockerfile
   $ curl https://gitlab.com/mmoreau/docker-laravel/raw/master/dev/docker-compose.yml -o docker-compose.yml
   $ sed -i -e '/ARG APP_UID=/cARG APP_UID='$(id -u) -e '/ARG APP_GID=/cARG APP_GID='$(id -g) app.dockerfile
   $ docker-compose build
   $ docker-compose up
```


Now that we have launched our containers with the command "**docker-compose up**" or "**docker-compose up -d**", we can access the following services in the web browser 

|   Service   |          URL          |
|:-----------:|:---------------------:|
| Application | http://localhost:8000 |
| PHPMyAdmin  | http://localhost:8001 |

|   Service  |  User | Password  |
|:----------:|:-----:|:---------:|
| PHPMyAdmin |  user |  user123  |
| PHPMyAdmin |  root |  root123  |


You can change the information in the "**docker-compose.yml**" according to your needs, before doing this check that "**docker-compose up**" is not launched 

Let's get into the container where Laravel is installed, to be able to enter a container, the container must be running on the host machine otherwise you will not be able to access it in the case where we use "docker-compose".

There are several ways to perform actions inside a container, depending on what suits you best, either you use the interactive mode or you do not use the interactive mode

Here is an example of some of the orders, do not hesitate to consult Laravel's documentation (https://laravel.com/docs/6.x)

```
   $ docker exec -it app /bin/sh
   $ cd /var/www/html
   $ php artisan make:controller myController 
   $ php artisan make:request myRequest
   $ php artisan make:model myModel -m
   $ composer require laravel/ui --dev
   $ php artisan ui vue --auth
   $ exit
```

OR 

```
   $ docker exec app ls -A
   $ docker exec app php artisan make:controller myController
   $ docker exec app php artisan make:request myRequest
   $ docker exec app php artisan make:model myModel -m
   $ docker exec app composer require laravel/ui --dev
   $ docker exec app php artisan ui vue --auth
```

Modify the following lines in the "**.env**" file at the root of the project 

|        .env         |       Remark                                               |        
|---------------------|------------------------------------------------------------|
| DB_CONNECTION=mysql | We work with a MySQL image                                 |
| DB_HOST=mysql       | We want to connect to a MySQL server when we make requests |
| DB_PORT=3306        | See docker-compose.yaml                                    |
| DB_DATABASE=webdb   | See docker-compose.yaml                                    |
| DB_USERNAME=root    | See docker-compose.yaml                                    |
| DB_PASSWORD=root123 | See docker-compose.yaml                                    |

```
   $ docker exec app php artisan migrate
```

**Perform** a **backup** of the "**webdb**" database only

```
   $ docker exec mysql /usr/bin/mysqldump -u root --password=root123 webdb > backup_webdb.sql 2>/dev/null
```

**Restores** the **backup** of the "**webdb**" database in the MySQL container

```
   $ cat backup_webdb.sql | docker exec -i mysql /usr/bin/mysql -u root --password=root123 webdb 2>/dev/null
```

Make a backup of the entire database 

```
   $ docker exec mysql /usr/bin/mysqldump -u root --password=root123 --all-databases > backup_all.sql 2>/dev/null
```

To stop the container, do this
```
   $ docker-compose down
```

## Links

*   https://docs.docker.com/compose/reference/build/
*   https://docs.docker.com/compose/compose-file/
*   https://docs.docker.com/engine/reference/commandline/docker/
*   https://hub.docker.com/
*   https://packagist.org/packages/laravel/laravel
*   https://github.com/docker/compose/releases