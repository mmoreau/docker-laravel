FROM php:7.4.1-fpm-alpine3.11

RUN apk update && apk upgrade
RUN docker-php-ext-install pdo_mysql 
RUN apk add composer
RUN apk add nodejs npm

ARG APP_UID=1000
ARG APP_GID=1000

RUN addgroup -g $APP_GID -S $APP_UID
RUN adduser -u $APP_UID -S $APP_UID -G $APP_GID

WORKDIR /var/www/html/

COPY ./artisan artisan
COPY ./server.php server.php

RUN chown -R $APP_UID:$APP_GID .

USER $APP_GID